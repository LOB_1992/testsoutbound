﻿using AutomationPraticeWebSiteTests.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationPraticeWebSiteTests.CommonSteps
{
    public class Common : TestBase
    {
        public void Login()
        {
            AuthenticationPageObjects SignIn = new AuthenticationPageObjects();
            SignIn.ClickSignIn();
            SignIn.SetEmailLogin("test6NfIsu1F@mailinator.com");
            SignIn.SetPassword("TestJoseph123");
            SignIn.ClickButtonSignIn();
            NUnit.Framework.Assert.AreEqual("MY ACCOUNT", SignIn.GetTitleMyAccount());
        }
    }
}
