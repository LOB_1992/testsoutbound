﻿using System;
using NUnit.Framework;
using System.Threading;
using AutomationPraticeWebSiteTests.PageObjects;

namespace AutomationPraticeWebSiteTests.Tests
{
    [TestFixture]
    public class AccountTests : TestBase
    {
        [Test]
        public void CreateAccount()
        {
            string Nome = Convert.ToBase64String(Guid.NewGuid().ToByteArray()).Substring(1, 8);
            AuthenticationPageObjects SignIn = new AuthenticationPageObjects();
            Thread.Sleep(1000);
            SignIn.ClickSignIn();
            Thread.Sleep(1000);
            Assert.AreEqual("AUTHENTICATION", SignIn.GetTitlePage());
            SignIn.SetEmailCreate("test"+ Nome + "@mailinator.com");
            SignIn.ClickButtonCreateAnAccount();
            Thread.Sleep(10000);
            Assert.AreEqual("CREATE AN ACCOUNT", SignIn.GetTitlePageDetailsCreateAccount());
            SignIn.ClickRadioMr();
            SignIn.SetPersonalFirstName("Joseph");
            SignIn.SetPersonalLastName("Speas");
            SignIn.SetPersonalPassword("TestJoseph123");
            SignIn.SetAddress("37 E. Victoria St");
            SignIn.SetCity("Santa Barbara");
            SignIn.SelectState("California");
            SignIn.SetZipCode("93101");
            SignIn.SetMobilePhone("2129500001");
            SignIn.SetAddressAlias("1316 State St");
            SignIn.ClickButtonRegister();
            Assert.AreEqual("MY ACCOUNT", SignIn.GetTitleMyAccount());
        }

        [Test]
        public void CreateAccountWithoutEmail()
        {
            AuthenticationPageObjects SignIn = new AuthenticationPageObjects();
            Thread.Sleep(1000);
            SignIn.ClickSignIn();
            Thread.Sleep(1000);
            Assert.AreEqual("AUTHENTICATION", SignIn.GetTitlePage());
            Thread.Sleep(1000);
            SignIn.ClickSignIn();
            Thread.Sleep(1000);
            SignIn.ClickButtonCreateAnAccount();
            Thread.Sleep(1000);
            Assert.AreEqual("Invalid email address.", SignIn.GetErrorMessagemWithoutEmail());
        }
    }
}
