﻿using AutomationPraticeWebSiteTests.PageObjects;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutomationPraticeWebSiteTests.CommonSteps;

namespace AutomationPraticeWebSiteTests.Tests
{
    [TestFixture]
    public class LoginTests : TestBase
    {

        [Test]
        public void ValidLogin()
        {
            Common Step = new Common();
            Step.Login();
        }

        [Test]
        public void InvalidEmailLogin()
        {
            string Nome = Convert.ToBase64String(Guid.NewGuid().ToByteArray()).Substring(1, 8);
            AuthenticationPageObjects SignIn = new AuthenticationPageObjects();
            SignIn.ClickSignIn();
            SignIn.SetEmailLogin("test" + Nome + "F@mailinator.com");
            SignIn.SetPassword("TestJoseph123");
            SignIn.ClickButtonSignIn();
            Assert.AreEqual("Authentication failed.", SignIn.GetMessageErrorAuthenticationFailed());
        }

        [Test]
        public void LoginWithoutPassword()
        {
            string Nome = Convert.ToBase64String(Guid.NewGuid().ToByteArray()).Substring(1, 8);
            AuthenticationPageObjects SignIn = new AuthenticationPageObjects();
            SignIn.ClickSignIn();
            SignIn.SetEmailLogin("test" + Nome + "F@mailinator.com");
            SignIn.ClickButtonSignIn();
            Assert.AreEqual("Password is required.", SignIn.GetMessageErrorPasswordRequered());
        }
    }
}
