﻿using NUnit.Framework;
using AutomationPraticeWebSiteTests.CommonSteps;
using AutomationPraticeWebSiteTests.PageObjects;
using System.Threading;
using System;

namespace AutomationPraticeWebSiteTests.Tests
{
    [TestFixture]
    public class ShoppingCartTests : TestBase
    {
        [Test]
        public void BuyAProduct()
        {
            Common Step = new Common();
            Step.Login();
            string Quantity = "2";

            MainPageObjects Shop = new MainPageObjects();
            Shop.SetFindItem("T-Shirt");
            Shop.ClickButtonFind();
            Thread.Sleep(2000);

            Shop.ClickOnFoundProduct();
            Thread.Sleep(1000);

            string DescriptionItem = Shop.GetDescriptionItem();
            Shop.SetQuantityItem(Quantity);

            CartPageObjects Cart = new CartPageObjects();
            Cart.ClickButtonAddToCard();
            Thread.Sleep(1000);

            Assert.AreEqual(DescriptionItem, Cart.GetTitleAddCart());
            Cart.ClickButtonProceedToCheckout();
            Assert.AreEqual(DescriptionItem, Cart.GetTitleItemInCart());
            string UnitPrice = Cart.GetItemUnitPrice();
            double TotalItem = Convert.ToDouble(UnitPrice.Substring(1).Replace('.',',')) * Convert.ToDouble(Quantity);

            Assert.AreEqual(TotalItem.ToString(), Cart.GetTotalItem().Replace('.',',').Substring(1));

            string ShippingCart = Cart.GetShipping().Substring(1);
            double TotalShop = TotalItem + Convert.ToDouble(ShippingCart.Replace('.', ','));

            Assert.AreEqual(Convert.ToString(TotalShop).Replace(',', '.'), Cart.GetTotalPrice().Substring(1));

            Cart.ClickButtonProceedToCheckoutAfterCart();

            ShippingPageObjects Shipping = new ShippingPageObjects();

            Shipping.ClickButtonProceedToCheckoutAfterAdress();
            Thread.Sleep(1000);
            Assert.AreEqual(ShippingCart, Shipping.GetShippingPrice().Substring(1));
            Shipping.ClickCheckAgreeTermsOfService();
            Shipping.ClickButtonProceedToCheckoutAfterShipping();
            Thread.Sleep(1000);

            PaymentPageObjects Payment = new PaymentPageObjects();

            Payment.ClickChoosePayment();
            Assert.AreEqual(TotalShop.ToString().Replace(',','.'), Payment.GetAmountPayment().Substring(1));

            Payment.ClickIConfirmMyOrder();
            Thread.Sleep(1000);

            OrderConfirmationPageObjects Confirmation = new OrderConfirmationPageObjects();
            Assert.AreEqual("ORDER CONFIRMATION", Confirmation.GetTitleOrderConfirmation());
            Assert.AreEqual("Your order on My Store is complete.", Confirmation.GetMessageOrderConfirmation());
            Assert.AreEqual(TotalShop.ToString().Replace(',', '.'), Confirmation.GetAmountOrderConfirmation().Substring(1));
        }

        [Test]
        public void DeleteItemInCart()
        {
            Common Step = new Common();
            Step.Login();
            string Quantity = "2";

            MainPageObjects Shop = new MainPageObjects();
            Shop.SetFindItem("T-Shirt");
            Shop.ClickButtonFind();
            Thread.Sleep(2000);

            Shop.ClickOnFoundProduct();
            Thread.Sleep(1000);

            string DescriptionItem = Shop.GetDescriptionItem();
            Shop.SetQuantityItem(Quantity);

            CartPageObjects Cart = new CartPageObjects();
            Cart.ClickButtonAddToCard();
            Thread.Sleep(1000);

            Assert.AreEqual(DescriptionItem, Cart.GetTitleAddCart());
            Cart.ClickButtonProceedToCheckout();
            Thread.Sleep(1000);
            Cart.ClickButtonDeleteItem();
            Thread.Sleep(1000);
            Assert.AreEqual("Your shopping cart is empty.", Cart.GetMessageEmptyCart());
        }
    }
}
