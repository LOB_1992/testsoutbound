﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationPraticeWebSiteTests.PageObjects
{
    public class PaymentPageObjects : TestBase
    {
        public void ClickChoosePayment()
        {
            Driver.FindElement(By.XPath(@"//*[@id='HOOK_PAYMENT']/div[1]/div/p/a")).Click();
        }

        public string GetAmountPayment()
        {
            return Driver.FindElement(By.Id("amount")).Text;
        }

        public void ClickIConfirmMyOrder()
        {
            Driver.FindElement(By.XPath(@"//*[@id='cart_navigation']/button/span")).Click();
        }
    }
}
