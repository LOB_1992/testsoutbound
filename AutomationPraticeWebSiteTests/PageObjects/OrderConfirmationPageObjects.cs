﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationPraticeWebSiteTests.PageObjects
{
    public class OrderConfirmationPageObjects : TestBase
    {
        public string GetTitleOrderConfirmation()
        {
            return Driver.FindElement(By.XPath(@"//*[@id='center_column']/h1")).Text;
        }

        public string GetMessageOrderConfirmation()
        {
            return Driver.FindElement(By.XPath(@"//*[@id='center_column']/div/p/strong")).Text;
        }

        public string GetAmountOrderConfirmation()
        {
            return Driver.FindElement(By.XPath(@"//*[@id='center_column']/div/span/strong")).Text;
        }
    }
}
