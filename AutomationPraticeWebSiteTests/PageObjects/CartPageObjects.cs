﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationPraticeWebSiteTests.PageObjects
{
    public class CartPageObjects : TestBase
    {
        public void ClickButtonAddToCard()
        {
            Driver.FindElement(By.Name("Submit")).Click();
        }

        public string GetTitleAddCart()
        {
            return Driver.FindElement(By.XPath(@"//*[@id='center_column']/div/div/div[3]/h1")).Text;
        }

        public void ClickButtonProceedToCheckout()
        {
            Driver.FindElement(By.XPath(@"//*[@id='layer_cart']/div[1]/div[2]/div[4]/a")).Click();
        }

        public string GetTitleItemInCart()
        {
            return Driver.FindElement(By.XPath(@"//*[@id='product_1_1_0_32046']/td[2]/p/a")).Text;
        }

        public string GetItemUnitPrice()
        {
            return Driver.FindElement(By.Id("product_price_1_1_32046")).Text;
        }

        public string GetTotalItem()
        {
            return Driver.FindElement(By.Id("total_product")).Text;
        }

        public string GetShipping()
        {
            return Driver.FindElement(By.Id("total_shipping")).Text;
        }

        public string GetTotalPrice()
        {
            return Driver.FindElement(By.Id("total_price")).Text;
        }

        public void ClickButtonProceedToCheckoutAfterCart()
        {
            Driver.FindElement(By.XPath(@"//*[@id='center_column']/p[2]/a[1]")).Click();
        }

        public void ClickButtonDeleteItem()
        {
            Driver.FindElement(By.Id("1_1_0_32046")).Click();
        }

        public string GetMessageEmptyCart()
        {
            return Driver.FindElement(By.XPath(@"//*[@id='center_column']/p")).Text;
        }

        public void ClickContinueShopping()
        {
            Driver.FindElement(By.XPath(@"//*[@id='center_column']/p[2]/a[2]")).Click();
        }
    }
}
