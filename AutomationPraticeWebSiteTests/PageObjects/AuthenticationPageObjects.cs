﻿using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace AutomationPraticeWebSiteTests.PageObjects
{
    public class AuthenticationPageObjects : TestBase
    {
        public AuthenticationPageObjects()
        {
            PageFactory.InitElements(TestBase.Driver, this);
        }

        public void ClickSignIn()
        {
            var SignIn = Driver.FindElement(By.XPath(@"//*[@id='header']/div[2]/div/div/nav/div[1]/a"));
            SignIn.Click();
        }

        public string GetTitlePage()
        {
            return Driver.FindElement(By.XPath(@"//*[@id='center_column']/h1")).Text;
        }

        public void SetEmailCreate(string Email)
        {
            Driver.FindElement(By.Id("email_create")).SendKeys(Email);
        }

        public void ClickButtonCreateAnAccount()
        {
            Driver.FindElement(By.Id("SubmitCreate")).Click();
        }

        public string GetTitlePageDetailsCreateAccount()
        {
            return Driver.FindElement(By.XPath(@"//*[@id='noSlide']/h1")).Text;
        }

        public void ClickRadioMr()
        {
            Driver.FindElement(By.Id("id_gender1")).Click();
        }

        public void ClickRadioMrs()
        {
            Driver.FindElement(By.Id("id_gender2")).Click();
        }

        public void SetPersonalFirstName(string FirstName)
        {
            Driver.FindElement(By.Id("customer_firstname")).SendKeys(FirstName);
        }

        public void SetPersonalLastName(string LastName)
        {
            Driver.FindElement(By.Id("customer_lastname")).SendKeys(LastName);
        }

        public void SetPersonalPassword(string Password)
        {
            Driver.FindElement(By.Id("passwd")).SendKeys(Password);
        }

        public void SetFirstName(string FirstName)
        {
            Driver.FindElement(By.Id("firstname")).SendKeys(FirstName);
        }

        public void SetLastName(string LastName)
        {
            Driver.FindElement(By.Id("lastName")).SendKeys(LastName);
        }

        public void SetAddress(string Address)
        {
            Driver.FindElement(By.Id("address1")).SendKeys(Address);
        }

        public void SetCity(string City)
        {
            Driver.FindElement(By.Id("city")).SendKeys(City);
        }

        public void SelectState(string State)
        {
            var state = Driver.FindElement(By.Id("id_state"));
            var SelectElement = new SelectElement(state);

            SelectElement.SelectByText(State);
        }

        public void SetZipCode(string ZipCode)
        {
            Driver.FindElement(By.Id("postcode")).SendKeys(ZipCode);
        }

        public void SelectCountry(string Country)
        {
            var country = Driver.FindElement(By.Id("id_country"));
            var SelectElement = new SelectElement(country);

            SelectElement.SelectByValue(Country);
        }

        public void SetMobilePhone(string Phone)
        {
            Driver.FindElement(By.Id("phone_mobile")).SendKeys(Phone);
        }

        public void SetAddressAlias(string AddressAlias)
        {
            Driver.FindElement(By.Id("alias")).SendKeys(AddressAlias);
        }

        public void ClickButtonRegister()
        {
            Driver.FindElement(By.Id("submitAccount")).Click();
        }

        public string GetTitleMyAccount()
        {
            return Driver.FindElement(By.XPath(@"//*[@id='center_column']/h1")).Text;
        }

        public string GetErrorMessagemWithoutEmail()
        {
            return Driver.FindElement(By.Id("create_account_error")).Text;
        }

        public void SetEmailLogin(string Email)
        {
            Driver.FindElement(By.Id("email")).SendKeys(Email);
        }

        public void SetPassword(string Password)
        {
            Driver.FindElement(By.Id("passwd")).SendKeys(Password);
        }

        public void ClickButtonSignIn()
        {
            Driver.FindElement(By.Id("SubmitLogin")).Click();
        }

        public string GetMessageErrorAuthenticationFailed()
        {
            return Driver.FindElement(By.XPath(@"//*[@id='center_column']/div[1]/ol/li")).Text;
        }

        public string GetMessageErrorPasswordRequered()
        {
            return Driver.FindElement(By.XPath(@"//*[@id='center_column']/div[1]/ol/li")).Text;
        }
    }
}
