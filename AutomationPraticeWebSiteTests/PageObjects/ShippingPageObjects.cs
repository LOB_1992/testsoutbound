﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationPraticeWebSiteTests.PageObjects
{
    public class ShippingPageObjects : TestBase
    {
        public void ClickButtonProceedToCheckoutAfterAdress()
        {
            Driver.FindElement(By.XPath(@"//*[@id='center_column']/form/p/button")).Click();
        }

        public string GetShippingPrice()
        {
            return Driver.FindElement(By.XPath(@"//*[@id='form']/div/div[2]/div[1]/div/div/table/tbody/tr/td[4]/div")).Text;
        }

        public void ClickCheckAgreeTermsOfService()
        {
            Driver.FindElement(By.Id("cgv")).Click();
        }

        public void ClickButtonProceedToCheckoutAfterShipping()
        {
            Driver.FindElement(By.Name("processCarrier")).Click();
        }
    }
}
