﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AutomationPraticeWebSiteTests.PageObjects
{
    public class MainPageObjects : TestBase
    {
        public MainPageObjects()
        {
            PageFactory.InitElements(TestBase.Driver, this);
        }

        public void SetFindItem(string Item)
        {
            Driver.FindElement(By.Id("search_query_top")).Clear();
            Driver.FindElement(By.Id("search_query_top")).SendKeys(Item);
        }

        public void ClickButtonFind()
        {
            Driver.FindElement(By.Name("submit_search")).Click();
        }

        public string GetDescriptionItem()
        {
            return Driver.FindElement(By.XPath(@"//*[@id='center_column']/div/div/div[3]/h1")).Text;
        }

        public void SetQuantityItem(string Quantity)
        {
            Driver.FindElement(By.Id("quantity_wanted")).Clear();
            Driver.FindElement(By.Id("quantity_wanted")).SendKeys(Quantity);
        }

        public void ClickOnFoundProduct()
        {
            Driver.FindElement(By.XPath(@"//*[@id='center_column']/ul/li/div/div[2]/h5/a")).Click();
        }
    }
}
