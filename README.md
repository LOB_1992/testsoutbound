# README #

The tests were developed in C# and was used some frameworks: Selenium, Nunit e ExtentReport.
The tool used for developed the tests was Visual Studio 2017 Community.

It's necessary to create a folder "C:\LogsSelenium\Log".

No additional configuration it is necessary, because the Project is configured with NuGet.

### How to run tests? ###

There are two ways for running the tests. The first is from Visual Studio. And the second is from Command line using Nunit Console. 
For more details read more in http://nunit.org/docs/2.6/consoleCommandLine.html 

### Tests Strutures ###

The tests struture has five folders:
-Base: There, it's possible to find a class where initialize the WebDriver (ChromeDriver) and ExtentReport;
-CommonSteps: There is all of common steps usually used on the tests.
-PageObjects: There is all of objects map of the web pages.
-Report: There is a ExtentReport methods.
-Tests: There are all the implementation tests.